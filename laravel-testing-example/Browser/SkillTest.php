<?php

namespace Tests\Browser;

use App\User;
use App\Serie;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SkillTest extends DuskTestCase
{
    /** @test */
    public function user_login_and_redirect_to_home_page()
    {
        $this->actingAs($this->superUserLoginIn())->browse(function (Browser $browser) {
            $browser->visit(route('login'))
                    ->type('email', auth()->user()->email)
                    ->type('password', '123456')
                    ->press('Login')
                    ->assertPathIs('/home');
        });
    }

    /** @test */
    public function create_skill_button_link()
    {
        $this->actingAs($this->superUserLoginIn())->browse(function (Browser $browser)  {
            $browser->visit(route('skill.create'))
                ->click('#skill-create-button')
                ->assertSee('Create New');
        });
    }
}
