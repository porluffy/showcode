<?php

namespace Tests\Feature;

use App\User;
use App\Serie;
use App\Skill;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminBarReadTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->skill = create(Skill::class);
    }

    /** @test */
    public function a_user_cannot_see_admin_bar()
    {
        $this->signIn(create(User::class));
        $this->get(route('home'))
        ->assertDontSee('PG');
    }

    /** @test */
    public function super_user_can_see_admin_bar()
    {
        $this->signIn($this->user);
        $this->get(route('home'))
        ->assertDontSee('PG');
    }

}
