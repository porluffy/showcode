<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    /** @test */
    public function everyone_get_home_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
