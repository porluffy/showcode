<?php

namespace Tests\Feature\Screencast;

use App\User;
use App\Serie;
use App\Screencast;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScreencastReadTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->screencast = create(Screencast::class, [
            'user_id' => $this->superuser
        ]);
        
    }

    /** @test */
    public function frontend_should_have_screencast_detail_page()
    {
        $this->get(route('frontend.screencasts.detail', ['slug' => $this->screencast->slug]))
        ->assertStatus(200);
    }

    /** @test */
    public function frontend_screencast_detail_without_serie_should_see_about_it_info()
    {
        $screencast = create(Screencast::class, ['serie_id' => null]);
        $this->get(route('frontend.screencasts.detail', ['slug' => $screencast->slug]))
        ->assertSeeText($screencast->title)
        ->assertSeeText($screencast->created_at->format('F j, Y'))
        ->assertSeeText($screencast->content);
    }

    /** @test */
    public function screencast_should_be_filterable_by_title()
    {
        $this->signIn($this->superuser);
        create(Screencast::class,['title' => 'Test 1'], 40);
        create(Screencast::class, ['title' => 'PHP'], 10);
        $response = $this->get(route('screencast.index', ['title' => 'PHP']));
        $response->assertViewHas('screencasts');
        $this->assertEquals(10, count($response->original['screencasts']));

    }

    /** @test */
    public function screencast_should_be_filterable_by_serie()
    {
        $this->signIn($this->superuser);
        create(Screencast::class,[
            'title' => 'Test 1',
            'serie_id' => $this->serie->id
        ], 5);
        $response = $this->get(route('screencast.index', ['serie_id' => [$this->serie->id]]));
        $response->assertViewHas('screencasts');
        $this->assertEquals(5, count($this->getResponseData($response, 'screencasts')));

    }

    /** @test */
    public function screencast_should_be_filterable_by_multi_serie()
    {
        $this->signIn($this->superuser);
        $serie = create(Serie::class);
        create(Screencast::class,[
            'title' => 'Test 1',
            'serie_id' => $this->serie->id
        ], 5);
        create(Screencast::class,[
            'title' => 'Test 1',
            'serie_id' => $serie->id
        ], 5);
        $response = $this->get(route('screencast.index', ['serie_id' => [$this->serie->id, $serie->id]]));
        $response->assertViewHas('screencasts');
        $this->assertEquals(10, count($this->getResponseData($response, 'screencasts')));

    }

    /** @test */
    public function screencast_listing_could_be_filtered_by_no_serie()
    {
        $this->signIn($this->superuser);
        create(Screencast::class,[
            'title' => 'Test 1',
            'serie_id' => null
        ], 5);
        $response = $this->get(route('screencast.index', ['no_serie' => null, 'title' => 'Test 1']));
        $response->assertViewHas('screencasts');
        $this->assertEquals(5, count($this->getResponseData($response, 'screencasts')));

    }

    /** @test */
    public function a_screencast_might_have_no_series()
    {
        $screencast = create(Screencast::class,['serie_id' => null]);
        $this->actingAs($this->superuser)->get(route('screencast.index'))
        ->assertSee('No Serie');

    }

    /** @test */
    public function admin_should_see_screencast_listing()
    {
        $this->signIn($this->superuser);
        $response = $this->get(route('screencast.index'));
        $response->assertStatus(200);
    }

    /** @test */
    public function no_user_should_not_access_screencast_listing()
    {
        $this->signIn($this->user);
        $this->get(route('screencast.index'))
        ->assertRedirect(route('home'));
    }
    
    /** @test */
    public function super_user_should_see_create_button_in_listing()
    {
        $this->signIn($this->superuser);
        $this->get(route('screencast.index'))
        ->assertSeeText('Create New');
    }


    /** @test */
    public function screencast_listing_has_screencast_title()
    {
        $this->signIn($this->superuser);
        $this->get(route('screencast.index'))
        ->assertSeeText($this->screencast->title);
    }

    /** @test */
    public function screencast_listing_has_screencast_id()
    {
        $this->signIn($this->superuser);
        $this->get(route('screencast.index'))
        ->assertSeeText($this->screencast->id);
    }

    /** @test */
    public function screencast_listing_has_serie_name()
    {
        $screencast = create(Screencast::class, [
            'user_id' => $this->superuser,
            'serie_id' => $this->serie
        ]);
        $this->signIn($this->superuser);
        $this->get(route('screencast.index'))
        ->assertSeeText($screencast->serie->title);
    }



}
