<?php

namespace Tests\Feature;

use App\User;
use App\Serie;
use App\Skill;
use Carbon\Carbon;
use App\Screencast;
use Tests\TestCase;
use App\Enums\Status;
use App\Enums\ScreencastType;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use BenSampo\Enum\Exceptions\InvalidEnumMemberException;

class ScreencastCreateTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);
        
    }

    /** @test */
    public function only_super_user_access_create_screenshot_page()
    {
        $this->signIn($this->superuser);
        $this->get(route('screencast.create'))
        ->assertStatus(200);
    }


    /** @test */
    public function no_user_should_not_access_create_screenshot_page()
    {
        $this->signIn($this->user);
        $this->get(route('screencast.create'))
        ->assertRedirect(route('home'));
    }

    /** @test */
    public function create_new_screencast()
    {
        $this->signIn($this->superuser);
        $screencast = make(Screencast::class, [
            'user_id' => auth()->user()->id,
            'title' => 'This is job title',
            'content' => 'This is job content',
            'serie_id' => $this->serie->id
        ]);

        $this->post(route('screencast.store'), $screencast->toArray());

        $this->assertEquals("This is job title", $screencast->title);

    }


    /** @test */
    public function create_new_screencast_without_serie()
    {
        $this->signIn($this->superuser);
        $screencast = make(Screencast::class, [
            'user_id' => auth()->user()->id,
            'title' => 'This is job title',
            'content' => 'This is job content',
        ]);

        $this->post(route('screencast.store'), $screencast->toArray());

        $this->assertEquals("This is job title", $screencast->title);

    }

    /** @test */
    public function a_screencast_required_title()
    {
        $this->newScreencast(['title' => null])
        ->assertSessionHasErrors('title');
    }


    /** @test */
    public function a_screencast_required_content()
    {
        $this->newScreencast(['content' => null])
        ->assertSessionHasErrors('content');
    }

    /** @test */
    public function a_screencast_without_serie()
    {
        $this->newScreencast(['serie_id' => null]);
        $this->assertDatabaseHas('screencasts', ['serie_id' => null]);
    }

    /** @test */
    public function a_screencast_with_serie()
    {
        $serie = create(Serie::class);
        $this->newScreencast(['serie_id' => $serie->id]);
        $this->assertDatabaseHas('screencasts', ['serie_id' => $serie->id]);
    }


    /** @test */
    public function a_screencast_with_invalid_serie()
    {
        $this->newScreencast(['serie_id' => 133434])
        ->assertSessionHasErrors('serie_id');
    }

    /** @test */
    public function a_screencast_required_level()
    {
            try {
                $this->newScreencast(['level' => 5])
                ->assertSessionHasErrors('level');
            } catch (InvalidEnumMemberException $exception) {
                $this->assertEquals($exception->getCode(), 0);
            }            
    }

    
    public function a_screencast_required_valid_level_id()
    {
        try {
            $this->newScreencast(['level' => 1, 'title' => 'Level testing']);
            $this->assertDatabaseHas('screencasts', ['level' => 1, 'title' => 'Level testing']);
        } catch (InvalidEnumMemberException $exception) {
            $this->assertEquals($exception->getCode(), 0);
        } 
    }

    /** @test */
    public function a_screencast_required_status()
    {
        $this->newScreencast(['status' => ''])
        ->assertSessionHasErrors('status');
    }

    /** @test */
    public function a_screencast_schedule_should_be_valid_datetime()
    {
        $fake_date = $this->faker()->dateTimeBetween('tomorrow', '+10 days');
        $date_value = $fake_date->format('Y-m-d H:i:s');
        $this->newScreencast(['scheduled_at' => $date_value]);
        $this->assertDatabaseHas('screencasts', ['scheduled_at' => $date_value]);
    }

    /** @test */
    public function a_screencast_scheduled_at_value_from_input()
    {
        $fake_date = Carbon::parse("2030-05-21 15:20:00");
        $this->newScreencast(['scheduled_at' => $fake_date->format('Y-m-d H:i:s')]);
        $this->assertDatabaseHas('screencasts', ['scheduled_at' => $fake_date->format('Y-m-d H:i:s') ]);
    }

    /** @test */
    public function a_screencast_required_valid_status()
    {
        $this->newScreencast(['status' => Status::Draft]);
        $this->assertDatabaseHas('screencasts', ['status' => Status::Draft]);
    }

    /** @test */
    public function a_screencast_required_valid_status_number()
    {
        $this->newScreencast(['status' => 'sdfsdf'])
        ->assertSessionHasErrors('status');
    }

    /** @test */
    public function a_screencast_status_no_need_to_be_integer_value()
    {
        $this->newScreencast(['status' => 1]);
        $this->assertDatabaseHas('screencasts', ['status' => Status::Published]);
    }

    /** @test */
    public function a_screencast_required_duration()
    {
        $this->newScreencast(['duration' => ''])
        ->assertSessionHasErrors('duration');
    }

    /** @test */
    public function a_screencast_required_skills()
    {
        $this->newScreencast(['skills' => ''])
        ->assertSessionHasErrors('skills');
    }

    /** @test */
    public function a_screencast_required_skills_field_should_be_array()
    {
        $this->newScreencast(['skills' => 'dfdf'])
        ->assertSessionHasErrors('skills');
    }

    /** @test */
    public function a_screencast_required_skills_field_should_be_valid_ids()
    {
        $this->newScreencast(['skills' => [234324324,324324324,4324234324]])
        ->assertSessionHasErrors('skills');
    }

    /** @test */
    public function a_screencast_duration_should_be_valid()
    {
        $this->newScreencast(['duration' => "sdfds"])
        ->assertSessionHasErrors('duration');
    }

    /** @test */
    public function create_sceencast_has_all_series_selectbox()
    {
        $this->signIn($this->superuser);
        $series = create(Serie::class,[], 30);
        $this->get(route('screencast.create'));
        $this->assertEquals(30, count($series));
    }

    /** @test */
    public function a_screencast_required_screencast_type()
    {
        $this->newScreencast(['screencast_type' => ""])
        ->assertSessionHasErrors('screencast_type');
    }

    /** @test */
    public function a_screencast_type_must_be_free_or_pay()
    {
        $this->newScreencast(['screencast_type' => "1"]);
        $this->assertDatabaseHas('screencasts', ['status' => ScreencastType::FREE]);
    }

    /** @test */
    public function attach_skills_when_creating_screencast()
    {
        create(Skill::class, [], 20);
        $screencast = create(Screencast::class);
        $screencast->skills()->sync([2,3,4]);
        $this->assertEquals(3, Screencast::find($screencast->id)->skills->pluck('id')->count());
    }


    public function newScreencast($overwrites = [])
    {
        session()->start();
        $this->signIn($this->superuser);
        // $screencast = make(Screencast::class, $overwrites)->toArray();
        $screencast = array_merge([
            'title' => 'Some title',
            'content' => 'Some content',
            'user_id' => auth()->user()->id,
            'serie_id' => factory(Serie::class)->make()->id, // $screencast->serie_id,
            'duration' => 3000,
            'status' => 1,
            'scheduled_at' => "2004-06-21 20:26:53",
            'level' => 1,
            'screencast_type' => 1,
            '_token' => csrf_token(),
            'skills' => create(Skill::class,[], 3)->pluck('id')->toArray(),
        ], $overwrites);
        return $this->post(route('screencast.store'), $screencast);
    }
    

}
