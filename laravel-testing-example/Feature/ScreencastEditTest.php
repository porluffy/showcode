<?php

namespace Tests\Feature;

use App\Screencast;
use App\User;
use App\Serie;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScreencastEditTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;
    private $screencast;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->screencast = create(Screencast::class);
        
    }

    /** @test */
    public function screencast_has_edit_page_link()
    {
        $this->signIn($this->superuser);
        $this->get(route('screencast.edit', ['screencast' => $this->screencast]))
        ->assertStatus(200)
        ->assertSeeText('Screencast Edit');
    }

    /** @test */
    public function user_should_not_get_screencast_edit_page_link()
    {
        $this->signIn($this->user);
        $this->get(route('screencast.edit', ['screencast' => $this->screencast]))
        ->assertStatus(302);
    }

    /** @test */
    public function screencast_edit_page_should_see_title_value()
    {
        $this->fillable_value_visible()
        ->assertSee($this->screencast->title);        

    }

    /** @test */
    public function screencast_edit_page_should_see_content_value()
    {
        $this->fillable_value_visible()
        ->assertSee($this->screencast->content);        

    }
    

    public function fillable_value_visible()
    {
        $this->signIn($this->superuser);
        return $this->get(route('screencast.edit', ['screencast' => $this->screencast]));
    }
}
