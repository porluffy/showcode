<?php

namespace Tests\Feature\Serie;

use App\User;
use App\Serie;
use App\Screencast;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SerieReadTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->screencast = create(Screencast::class, [
            'user_id' => $this->superuser
        ]);
        
    }
    
    
    /** @test */
    public function serie_search_route_by_title()
    {
        $this->signIn($this->superuser);
        create(Serie::class, ['title' => 'Material Design']);
        $this->get(route('serie.search', ['title' => 'Material Design']))
        ->assertJsonCount(1);        
    }


    /** @test */
    public function serie_search_route_by_ids()
    {
        $this->signIn($this->superuser);
        $requestSerieIds = create(Serie::class,['title' => 'Preloaded Serie Title'], 5)->pluck('id')->toArray();
        $response = $this->get(route('serie.search', ['ids' => $requestSerieIds]));
        $this->assertCount(5, $response->json('results'));
    }
}
