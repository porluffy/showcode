<?php

namespace Tests\Feature\Skill;

use App\User;
use App\Serie;
use App\Skill;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillCreateTest extends TestCase
{

    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

    }

    /** @test */
    public function skill_create_page()
    {
        $this->signIn($this->superuser);
        $response = $this->get(route('skill.create'));
        $response->assertStatus(200);
    }

    /** @test */
    public function save_new_skill()
    {
        $this->signIn($this->superuser);
        $skill = $this->dummyNewSkill();
        $newSkill = Skill::create($skill); 
        
        $this->assertDatabaseHas('skills', [
            'name' => $newSkill->name
        ]);          
    }

    /** @test */
    public function skill_create_redirect_to_listing_after_success()
    {
        $this->signIn($this->superuser);
        $this->requestNewSkill()
        ->assertStatus(200);
    }

    /** @test */
    public function skill_listing_first_page_show_new_skill()
    {
        create(Skill::class,[], 40);
        $this->signIn($this->superuser);
        $this->requestNewSkill(['name' => 'blue rock']);
        $response = $this->get(route('skill.list'));
        $response->assertStatus(200);
         $response->assertJson([
            'data' => [
               [
                  'name' => 'blue rock'
               ]
            ]
         ]);
    }

    /** @test */
    public function skill_should_save_with_unique_slug()
    {
        $this->signIn($this->superuser);
        $this->requestNewSkill(['slug' => 'css']);
        
        $this->assertDatabaseHas('skills', [
            'slug' => 'css'
        ]);          
    }

    /** @test */
    public function new_skill_required_slug_unique()
    {

        $skill = create(Skill::class,['slug' => 'duplication-slug']);
        $this->signIn($this->superuser);
        
        $this->requestNewSkill(['slug' => $skill->slug])
            ->assertSessionHasErrors(['slug']);
    }

    /** @test */
    public function new_skill_required_name()
    {
        $this->signIn($this->superuser);
        $this->requestNewSkill(['name' => ''])
            ->assertSessionHasErrors(['name']);
    }


    public function dummyNewSkill($overwrites = [])
    {
        session()->start();
        $skill = make(Skill::class);
        $this->signIn($this->superuser);
        return  array_merge([
            'name' => $skill->name,
            'slug' => Str::slug($skill->name),
            'description' => $skill->description,
            '_token' => csrf_token(),
        ], $overwrites);
    }
    
    public function requestNewSkill($overwrites = [])
    {
        $this->signIn($this->superuser);
        $data = $this->dummyNewSkill($overwrites);
        return $this->post(route('skill.store'), $data);
    }

}
