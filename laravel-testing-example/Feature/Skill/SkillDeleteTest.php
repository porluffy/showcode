<?php

namespace Tests\Feature\Skill;

use App\User;
use App\Serie;
use App\Skill;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillDeleteTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->skill = create(Skill::class);
    }
    
    /** @test */
    public function skill_trash_link()
    {
        session()->start();
        $this->signIn($this->superuser);
        $response = $this->delete(route('skill.destroy', ['skill' => $this->skill]), ['_token' => csrf_token()]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'You trashed the record.']);
    }

    /** @test */
    public function skill_soft_delete()
    {
        session()->start();
        $this->signIn($this->superuser);
        $this->delete(route('skill.destroy', ['skill' => $this->skill]), ['_token' => csrf_token()]);
        $this->assertSoftDeleted('skills', [
            'name' => $this->skill->name,
            'description' => $this->skill->description,    
        ]);

    }

    /** @test */
    public function skill_soft_delete_could_be_able_to_restore()
    {
        $this->signIn($this->superuser);
        $skill = create(Skill::class,['deleted_at' => now()]);
        $this->put(route('skill.restore', ['id' => $skill->id]));
        $this->assertDatabaseHas('skills', [
            'id' => $skill->id,
            'deleted_at' => null,
        ]);
    }

}
