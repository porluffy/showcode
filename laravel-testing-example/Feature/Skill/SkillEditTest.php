<?php

namespace Tests\Feature\Skill;

use App\User;
use App\Serie;
use App\Skill;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillEditTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->skill = create(Skill::class);
    }


    /** @test */
    public function superuser_should_access_skill_edit_page()
    {
        $this->signIn($this->superuser);
        $this->get(route('skill.edit', ['skill' => $this->skill]))
        ->assertStatus(200);
    }

    /** @test */
    public function noraml_user_should_not_access_skill_edit_page()
    {
        $this->signIn($this->user);
        $this->get(route('skill.edit', ['skill' => $this->skill]))
        ->assertRedirect(route('home'));
    }

    /** @test */
    public function skill_update_required_name()
    {
        $this->signIn($this->superuser);
        $this->updateSkill(['name' => ''])
        ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function skill_update_with_same_slug()
    {
        $this->signIn($this->superuser);
        $this->updateSkill(['slug' => 'css-design'])
        ->assertSessionDoesntHaveErrors(['slug']);
    }

    /** @test */
    public function skill_update_should_not_accept_existing_slug()
    {
        session()->start();
        $this->signIn($this->superuser);
        create(Skill::class, ['slug' => 'sass']);
        $skill = create(Skill::class, ['slug' => 'design-material-network']);
        $update_skill_value = array_merge($skill->toArray(), ['slug' => 'sass', '_token' => csrf_token()]);
        $response = $this->put(route('skill.update', ['skill' => $skill]), $update_skill_value);
        $response->assertSessionHasErrors(['slug']);
    }

    /** @test */
    public function skill_update_name()
    {
        $this->signIn($this->superuser);
        $this->updateSkill(['name' => 'new skill']);
        $this->assertDatabaseHas('skills', ['name' => 'new skill']);
    }

    /** @test */
    public function edit_page_should_see_old_data_of_skill()
    {
        $this->signIn($this->superuser);
        $response = $this->get(route('skill.edit', ['skill' => $this->skill]));
        $skill = $this->getResponseData($response, 'skill', 'single');
        $this->assertEquals($this->skill->name, $skill->name);
    }
    
    public function updateSkill($overwrites = [])
    {
        session()->start();
        $this->signIn($this->superuser);
        $skill = create(Skill::class, $overwrites);
        $data = array_merge($skill->toArray(), ['_token' => csrf_token()]);
        return $this->put(route('skill.update', ['skill' => $skill]), $data);   
    }
}
