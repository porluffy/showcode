<?php

namespace Tests\Feature\Skill;

use App\User;
use App\Serie;
use App\Skill;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillReadTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->skill = create(Skill::class, ['name' => 'skillglobal']);
    }


    /** @test */
    public function skill_has_a_search_route()
    {
        $this->signIn($this->superuser);
        create(Skill::class, ['name' => 'Material Design']);
        $this->get(route('skill.search', ['name' => 'Material Design']))
        ->assertJsonCount(1);
    }


    /** @test */
    public function skill_should_be_filterable_by_name()
    {
        $this->signIn($this->superuser);
        create(Skill::class,['name' => 'Test 1'], 40);
        create(Skill::class, ['name' => 'DesignPattern'], 10);
        $response = $this->get(route('skill.list', ['name' => 'DesignPattern']));
        $response->assertStatus(200);
        $response->assertJsonCount(10, 'data');

    }

    /** @test */
    public function skill_should_be_able_to_filter_by_published()
    {
        $this->signIn($this->superuser);
        create(Skill::class,['name' => 'Test 1', 'deleted_at' => now()], 40);
        create(Skill::class, ['name' => 'PHP', 'deleted_at' => null], 10);

        $response = $this->get(route('skill.list', ['name' => 'PHP', 'deleted_at' => 'published']));
        $response->assertStatus(200);
        $response->assertJsonCount(10, 'data');
    }


    /** @test */
    public function skill_should_be_able_to_filter_by_trashed()
    {

        $this->signIn($this->superuser);
        create(Skill::class,['name' => 'Test 1', 'deleted_at' => now()], 40);
        $response = $this->get(route('skill.list', ['name' => 'Test 1', 'deleted_at' => 'trashed']));
        $response->assertStatus(200);
         $response->assertJsonCount(20, 'data');

    }
   
    /** @test */
    public function superuser_should_see_skill_listing()
    {
        $this->signIn($this->superuser);
        $response = $this->get(route('skill.index'));
        $response->assertStatus(200);
    }


    /** @test */
    public function normal_user_should_not_see_skill_listing()
    {
        $this->signIn($this->user);
        $response = $this->get(route('skill.index'))
        ->assertRedirect('home');
    }        
}
