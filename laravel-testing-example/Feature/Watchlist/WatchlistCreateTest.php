<?php

namespace Tests\Feature\Watchlist;

use App\Enums\Status;
use App\User;
use App\Serie;
use App\Screencast;
use App\Watchlist;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WatchlistCreateTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $superuser;
    private $serie;
    private $watchlist;


    public function setUp() : void
    {
        parent::setUp();
        $this->superuser = create(User::class, [
            'email' => 'hlaporthein@gmail.com'
        ]);

        $this->user = create(User::class, [
            'email' => 'john@gmail.com'
        ]);

        $this->serie = create(Serie::class);

        $this->screencast = create(Screencast::class, [
            'user_id' => $this->superuser
        ]);

        $this->watchlist = create(Watchlist::class);

    }

    /** @test */
    public function a_user_does_not_allow_duplicate_watchlist()
    {
        $this->signIn($this->superuser);
        $screencast = create(Screencast::class, [
            'status' => Status::Published
        ]);
        create(Watchlist::class, [
            'watchlistable_type' => "App\Screencast",
            'watchlistable_id' => $screencast->id,
            'user_id' => $this->superuser->id
        ]);
        $this->post(route('store.watchlist', ['id'  => $screencast->id, 'type' => 'screencast' ]))
        ->assertStatus(200)
        ->assertSeeText('Already in your watchlist');
    }

    /** @test */
    public function a_user_can_remove_screencast_watchlist()
    {
        $this->signIn($this->superuser);
        $screencast = create(Screencast::class, [
            'status' => Status::Published
        ]);
        create(Watchlist::class, [
            'watchlistable_type' => "App\Screencast",
            'watchlistable_id' => $screencast->id,
            'user_id' => $this->superuser->id
        ]);   
        $this->delete(route('destory.watchlist', ['id'  => $screencast->id, 'type' => 'screencast' ]))
        ->assertStatus(200);
        $this->assertDatabaseMissing('watchlists', [
            'user_id' =>  $this->superuser->id,
            'watchlistable_id' => $screencast->id,
        ]);
    }

    /** @test */
    public function user_has_to_add_before_remove_watchlist_item()
    {
        $this->signIn($this->superuser);
        $screencast = create(Screencast::class, [
            'status' => Status::Published
        ]);
        $this->delete(route('destory.watchlist', ['id'  => $screencast->id, 'type' => 'screencast' ]))
        ->assertStatus(400)
        ->assertSeeText('Add to your watchlist first');
    }

    /** @test */
    public function a_user_can_add_watchlist_for_screencast()
    {
        $this->signIn($this->superuser);
        $screencast = create(Screencast::class, [
            'status' => Status::Published
        ]);

        $this->post(route('store.watchlist', ['id'  => $screencast->id, 'type' => 'screencast' ]))
        ->assertStatus(200);
        $this->assertDatabaseHas('watchlists', [
            'user_id' =>  $this->superuser->id,
            'watchlistable_id' => $screencast->id,
        ]);
    }    

    /** @test */
    public function a_user_is_not_allowed_if_not_screencast_or_serie_for_watchlist()
    {
        $this->signIn($this->superuser);
        $this->post(route('store.watchlist', ['id'  => $this->screencast->id, 'type' => 'something' ]))
        ->assertStatus(400);
    }

    /** @test */
    public function user_is_only_allowed_for_published_screencast_when_adding_watchlist()
    {
        $this->signIn($this->superuser);
        $screencast = create(Screencast::class, [
            'status' => Status::Schedule
        ]);
        $this->post(route('store.watchlist', ['id'  => $screencast->id, 'type' => 'screencast' ]))
        ->assertStatus(400);
    }

    /** @test */
    public function user_is_only_allowed_for_published_serie_when_adding_watchlist()
    {
        $this->signIn($this->superuser);
        $serie = create(Serie::class, [
            'status' => Status::Schedule
        ]);
        $this->post(route('store.watchlist', ['id'  => $serie->id, 'type' => 'screencast' ]))
        ->assertStatus(404);
    }
    
}
