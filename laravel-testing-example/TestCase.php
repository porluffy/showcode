<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp() : void
    {
        parent::setUp();
//        $this->withoutExceptionHandling();
    }

    protected function signIn($user = null)
    {
        $user = $user ?: create(User::class);

        $this->actingAs($user);

        return $this;
    }

    /**
     * Get Response data Eg. view('page.about', ['products' => $products])
     * It would return *products* as a collection
     * If $responseType == single, it would return $product model
     *
     * @param Response $response
     * @param string $key
     * @return collection
     */
    protected function getResponseData($response, $key, $responseType = 'collection'){

        $content = $response->getOriginalContent();
        $content = $content->getData();
        $collection = collect($content[$key]->all());
        return ($responseType === "single") ? $collection->first() : $collection;
    }
}
