<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }
/**
 * Plugin Name: fly image crop
 * Plugin URI: http://hlaporthein.me
 * Description:  Fly Image Crop & Image Management On WordPress. It uses Aqua-Resizer Codes to crop.
 * Version: 1.0
 * Author: Hla Por Thein
 * Author URI: http://hlaporthein.me
 * License: GPL2+
 * Text Domain: vd
 */

define('INO_FIC_SAVE_FOLDER_NAME', 'fic-images');
define( 'INO_FIC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

/**
 * Plugin Init
 */
require plugin_dir_path(__FILE__) . 'inc/init.php';
if ( file_exists( plugin_dir_path(__FILE__) . 'sources/autoload.php' ) ) {

    require plugin_dir_path(__FILE__) . 'sources/autoload.php';
    add_action( 'plugins_loaded', 'ino_load_required_functionality_files', 100 );

} else {

    if ( file_exists( plugin_dir_path(__FILE__) . 'vendor/autoload.php' ) ) {

        require plugin_dir_path(__FILE__) . 'vendor/autoload.php';
        add_action( 'plugins_loaded', 'ino_load_required_functionality_files', 100 );

    } else {

        /**
         * Show message to install composer
         */
        add_action('wp_footer', function(){
            echo "Need to install composer in fly image crop plugin.";
        });

    }

}

/**
 * Activate require functions when activated plugins
 */
function ino_fic_plugin_activate() {

    set_transient( 'ino-fic-activate-plugin', true, 5 );
    ino_fic_create_fly_image_directory();
}

register_activation_hook( __FILE__, 'ino_fic_plugin_activate' );

/**
 * Activate require functions when deactivated plugins
 */
function ino_fic_plugin_deactivate() {

//    ino_fic_remove_fly_image_directory();
}

register_deactivation_hook( __FILE__, 'ino_fic_plugin_deactivate' );


/**
 * Load required functionality files
 */
function ino_load_required_functionality_files() {
    require plugin_dir_path(__FILE__) . 'inc/aq_resizer.php';
    require plugin_dir_path(__FILE__) . 'inc/helpers.php';
    require plugin_dir_path(__FILE__) . 'inc/hooks.php';
    require plugin_dir_path(__FILE__) . 'inc/filters.php';
}
