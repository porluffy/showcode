<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }


add_filter( 'media_row_actions', 'media_add_delete_link_in_row', 10, 2 );
function media_add_delete_link_in_row( $actions, $post ) {
    if ( 'image/' !== substr( $post->post_mime_type, 0, 6 ) || ! current_user_can( 'manage_options' ) ) {
        return $actions;
    }

    $url                         = wp_nonce_url( admin_url( 'upload.php?post_id=' . $post->ID ), 'delete_fly_crop_image_for_specific_image', 'fly_crop_image_nonce' );
    $actions['fly-image-delete'] = '<a href="' . esc_url( $url ) . '" title="' . esc_attr( __( 'Delete all fly crop image sizes for this image', 'inoforest' ) ) . '">' . __( 'Delete Fly Crop Image', 'inoforest' ) . '</a>';

    return $actions;
}
