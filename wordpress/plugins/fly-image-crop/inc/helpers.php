<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }


if ( !function_exists('ino_fic_resize') ) {
    function ino_fic_resize( $image_id, $width = null, $height = null, $crop = null, $single = true, $upscale = false ) {

        if ( !$image_id ) {
            return;
        }

        $url = wp_get_attachment_url($image_id);

        if ( defined( 'ICL_SITEPRESS_VERSION' ) ){
            global $sitepress;
            $url = $sitepress->convert_url( $url, $sitepress->get_default_language() );
        }

        $aq_resize = Aq_Resize::getInstance();
        return $aq_resize->process( $url, $width, $height, $crop, $single, $upscale );
    }

}


if ( !function_exists('ino_fic_crop_image_url') ) {
    function ino_fic_crop_image_url( $image_id, $width = null, $height = null) {
        return ino_fic_resize($image_id, $width, $height, true, true, true);
    }

}


if ( !function_exists('ino_fic_delete_crop_photos') ) {
    /**
     * Delete crop images for specific images
     *
     * @param $image_id
     */
    function ino_fic_delete_crop_photos( $image_id) {

        if ( !$image_id ) {
            return;
        }

        $meta_data = wp_get_attachment_metadata($image_id);
        $upload_dir = wp_upload_dir();
        $path = $upload_dir['basedir'] .'/' . $meta_data['file'];
        $fic_dir = $upload_dir['basedir'] .'/'. INO_FIC_SAVE_FOLDER_NAME;
        $pathinfo = pathinfo($path);
        $search_format = $fic_dir.'/'. $pathinfo['filename'].'*'. $pathinfo['extension'];

        $found_images = glob($search_format);

        foreach( $found_images as $image) {
            wp_delete_file($image);
        }

    }

}