<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }


add_action( 'admin_notices', 'ino_fic_activate_notification' );
function ino_fic_activate_notification(){
    if( get_transient( 'ino-fic-activate-plugin' ) ){
        ?>
        <div class="updated success is-dismissible">
            <p>Your fly crop image directory is <b><?php echo INO_FIC_SAVE_FOLDER_NAME; ?></b> inside the uploads folder.</p>
            <p style="display: none;">We would delete that folder if you uninstall this plugin!</p>
        </div>
        <?php
        delete_transient( 'ino-fic-activate-plugin' );
    }
}


/**
 * Delete cropped images for specific photo
 */
function ino_fic_delete_all_crop_images_for_specific_attachment() {


    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }

    if ( isset($_GET['delete_crop_dir_nonce']) && wp_verify_nonce( sanitize_key( $_GET['delete_crop_dir_nonce'] ), 'delete_crop_dir_action')) {

        add_action('admin_notices', function(){
            ino_fic_remove_fly_image_directory();
            ino_fic_create_fly_image_directory();
            echo '<div id="message" class="updated notice is-dismissible">
                    <p>We have deleted the cropped image folder. So, we have to crop each image when we view.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
        });

    } elseif ( isset($_GET['fly_crop_image_nonce']) && wp_verify_nonce( sanitize_key( $_GET['fly_crop_image_nonce'] ), 'delete_fly_crop_image_for_specific_image')) {
        $post = get_post(intval($_GET['post_id']));

        ino_fic_delete_crop_photos($post->ID);
        add_action('admin_notices', function($post){
            echo '<div id="message" class="updated notice is-dismissible">
                    <p>This photo auto cropped images are deleted. It would regenerate when you visit that photo post page.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
        });
    } else {
        return;
    }



}
add_action( 'load-upload.php', 'ino_fic_delete_all_crop_images_for_specific_attachment', 999);

add_action( 'restrict_manage_posts', 'add_admin_filters', 10, 1 );

function add_admin_filters( $post_type ){
    if( 'attachment' !== $post_type ){
        return;
    }

    $url  = wp_nonce_url( admin_url( 'upload.php?delete=all' ), 'delete_crop_dir_action', 'delete_crop_dir_nonce' );

    ?>
        <div class="alignright actions custom">
            <a href="<?php echo $url; ?>" name="custom_" style="height:32px;background: #f35e5e;color: #fff;border-color: #ce3636;" class="button" value=""><?php
                echo __( 'Delete All Dynamically Cropped Image', 'inoforest' ); ?></a>
        </div>
    <?php
}


