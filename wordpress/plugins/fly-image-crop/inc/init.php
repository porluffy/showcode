<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }

function ino_removeDirectory($path) {
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? ino_removeDirectory($file) : unlink($file);
    }
    rmdir($path);
    return;
}


/**
 * Create folder in sub directory of uploads folder
 */
function ino_fic_create_fly_image_directory() {
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/'. INO_FIC_SAVE_FOLDER_NAME;
    if (! is_dir($upload_dir)) {
        mkdir( $upload_dir, 0700 );
    }
}



/**
 * Create folder in sub directory of uploads folder
 */
function ino_fic_remove_fly_image_directory() {
    global $wp_filesystem;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/'. INO_FIC_SAVE_FOLDER_NAME;
    if (is_dir($upload_dir)) {
        ino_removeDirectory($upload_dir);
    }
}