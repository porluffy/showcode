<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}

if ( !defined('ENV') ) {
   define('ENV', 'pro'); //  define('ENV', ( $_SERVER['HTTP_HOST'] === "dev.test" ) ? 'dev' : 'pro');
   
}

$theme_info = wp_get_theme();
$static_file_version = ( ENV == 'pro' ) ? wp_get_theme()->get( 'Version' ) : time();

if ( !defined('ENV') ) {
    defined('ENV', 'pro');
}

define('INOFOREST_PRIMARY_COLOR', '#1a7def');
define('INOFOREST_SITE_URL', esc_url(home_url()));
define('INOFOREST_VERSION', $static_file_version);
define('INOFOREST_THEME_NAME', $theme_info->Name);
define('INOFOREST_THEME_LANGUAGE', 'inoforest');
define('INOFOREST_SHORTNAME', 'ino_');
define('INOFOREST_THEME_AUTHOR', $theme_info->Author);

define('INOFOREST_THEME_FOLDER_URL', get_template_directory_uri());
define("INOFOREST_THEME_FOLDER_ABSOLUTE_PATH", get_template_directory());
define("INOFOREST_PLUGIN_PACKAGE_ABSOLUTE_PATH", INOFOREST_THEME_FOLDER_ABSOLUTE_PATH . '/inc/plugins');



define('INOFOREST_THEME_ASSETS_URL', INOFOREST_THEME_FOLDER_URL . '/assets');
define('INOFOREST_THEME_JS_URL', INOFOREST_THEME_ASSETS_URL . '/js');
define('INOFOREST_THEME_IMG', INOFOREST_THEME_ASSETS_URL . '/img');


define('INOFOREST_THEME_DIST_URL', INOFOREST_THEME_FOLDER_URL . '/dist');
define('INOFOREST_THEME_DIST_IMG_URL', INOFOREST_THEME_DIST_URL . '/img');
define('INOFOREST_THEME_DIST_JS_URL', INOFOREST_THEME_DIST_URL . '/js');
define('INOFOREST_THEME_DIST_FONTS_URL', INOFOREST_THEME_DIST_URL . '/fonts');


define('INOFOREST_WEATHER_API_KEY', 'a8f3331d6f0bc34fc0852076cefed517');

define('INOFOREST_RANDOM_KEY', 'f3f03fj3f3jfoji3f3fj3fjf3okjfd');
