<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}

require_once get_template_directory() . '/config/theme-config.php';

require_once get_template_directory() . '/inc/InoforestHelpers.php';

require_once get_template_directory() . '/inc/InoforestTemplateHelper.php';

require_once get_template_directory() . '/inc/InoforestAction.php';

require_once get_template_directory() . '/inc/InoforestThemeSetup.php';

require_once get_template_directory() . '/inc/InoforestHook.php';

require_once get_template_directory() . '/inc/InoforestStatic.php';

require_once get_template_directory() . '/inc/InoforestPluginActivation.php';

require_once get_template_directory() . '/inc/InoforestFilter.php';

require_once get_template_directory() . '/inc/InoforestAcm.php';

/**
 * REQUIRED FILES
 * Include required files.
 */
//require get_template_directory() . '/inc/template-tags.php';