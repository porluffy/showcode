<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php wp_head(); ?>
</head>
<body <?php body_class('hide-mobile-menu ease-all-trans4') ?>>

<?php

InoforestAction::inoforest_header();
