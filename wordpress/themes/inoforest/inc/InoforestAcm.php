<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}

if (!class_exists('ACF')) {
	return;
}

/**
 * ACM Plugin File Included
 */
class InoforestAcm
{
	
	function __construct()
	{
        self::setting_page();
		self::include_all_files();
	}


	private static function include_all_files()
	{
	    InoforestHelpers::include_all_files(INOFOREST_THEME_FOLDER_ABSOLUTE_PATH . '/inc/acm');

    }
    
    private static function setting_page() {
        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' 	=> 'Homepage Settings',
                'menu_title'	=> 'Homepage Settings',
                'menu_slug' 	=> 'homepage-settings',
                'capability'	=> 'edit_posts',
                'redirect'		=> false
            ));
	
            acf_add_options_page(array(
                'page_title' 	=> 'Theme General Settings',
                'menu_title'	=> 'Theme Settings',
                'menu_slug' 	=> 'theme-general-settings',
                'capability'	=> 'edit_posts',
                'redirect'		=> false
            ));
            
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Advertisement',
                'menu_title'	=> 'advertisement',
                'parent_slug'	=> 'theme-general-settings',
            ));
            
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Theme Footer Settings',
                'menu_title'	=> 'Footer',
                'parent_slug'	=> 'theme-general-settings',
            ));
            
        }        
    }

}
new InoforestAcm();