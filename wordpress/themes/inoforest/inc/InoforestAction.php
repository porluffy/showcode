<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}


class InoforestAction {
    

    public static function inoforest_header()
    {
        do_action('inoforest_header');
    }

    public static function inoforest_footer()
    {
        do_action('inoforest_footer');
    }


    public static function inoforest_frontpage_content()
    {
        do_action('inoforest_frontpage_content');
    }

    public static function inoforest_single_content()
    {
        do_action('inoforest_single_content');
    }

    public static function inoforest_archive_content()
    {
        do_action('inoforest_archive_content');
    }

    public static function inoforest_page_content()
    {
        do_action('inoforest_page_content');
    }

    public static function inoforest_404_content()
    {
        do_action('inoforest_404_content');
    }

    public static function inoforest_search_content()
    {
        do_action('inoforest_search_content');
    }

}
new InoforestAction();