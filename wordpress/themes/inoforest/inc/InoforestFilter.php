<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}


class InoforestFilter {

    function __construct()
    {
        add_filter( 'get_archives_link',array( $this, 'archiveWidgetCountToLinkTag' ) );
    }

    public function archiveWidgetCountToLinkTag($links) {
        $links = str_replace('</a>&nbsp;(', '<span class="p-count">', $links);
        $links = str_replace(')', '</span></a>', $links);
        return $links;
    }

}
new InoforestFilter();