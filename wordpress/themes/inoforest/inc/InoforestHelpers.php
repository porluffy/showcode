<?php

if (!defined('ABSPATH')) {
    die('Direct access forbidden');
}

class InoforestHelpers
{

    public static function isMobile()
    {
        $detect = new Mobile_Detect;
        return ($detect->isMobile());
    }

    public static function isTablet()
    {
        $detect = new Mobile_Detect;
        return ($detect->isTablet());
    }

    public static function shortNumFormat($num)
    {
        if ($num > 1000) {

            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('K', 'M', 'B', 'T');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;
        }

        return $num;
    }

    public static function humanReadTimeForPost($post = 0)
    {
        return human_time_diff(get_the_time('U', $post), current_time('timestamp')) . ' Ago';
    }

    /**
     * @post_id Post id
     * @width crop image width size
     * @height crop image height size
     */
    public static function post_crop_image_url($post_id, $width, $height)
    {
        if (has_post_thumbnail($post_id)) {
            $thumbnail_id = get_post_thumbnail_id($post_id);
            return  ino_fic_crop_image_url($thumbnail_id, $width, $height);
        }
        return false;
    }

    /**
     * @terms Terms Objects Array
     * @obj_field Export terms to toArray with only specific field name
     */
    public static function term_object_to_arry($terms, $obj_field = "term_id")
    {
        if (!empty($terms)) {
            $term_list = [];
            foreach ($terms as $term) {
                $term_list[] = $term->$obj_field;
            }
            return $term_list;
        } else {
            return false;
        }
    }



    public static function file_url_with_version($url)
    {
        return $url . '?v=' . INOFOREST_VERSION;
    }

    public static function include_all_files($dir)
    {
        foreach (glob("{$dir}/*.php") as $filename) {
            include $filename;
        }
    }

    public static function include_all_files_with_subdir($dir, $args = [])
    {
        $exclude_dir = ( isset($args['excluded_dir']) ) ? $args['excluded_dir'] : [];
        $filter = function ($file, $key, $iterator) use ($exclude_dir) {
            if ($iterator->hasChildren() && !in_array($file->getFilename(), $exclude_dir)) {
                return true;
            }
            return $file->isFile();
        };
        $di = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        $it = new RecursiveIteratorIterator(new RecursiveCallbackFilterIterator($di, $filter));

        foreach ($it as $file) {
            if (pathinfo($file, PATHINFO_EXTENSION) == "php") {
                include $file->getPathname();
            }
        }
    }

    public static function ino_get_excerpt($post)
    {
      $post = (is_integer($post) ) ? get_post($post) : $post;
      if ( has_excerpt($post) ) {
         return get_the_excerpt($post);
      } else {
         return $post->post_content;
      }
    }

    public function currentQueryVars($wp_query = null, $args = [])
    {
        if (null === $wp_query) {
            global $wp_query;
        }
    }
}
