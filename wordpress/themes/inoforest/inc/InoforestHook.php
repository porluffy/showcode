<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}


class InoforestHook {

    function __construct()
    {
        add_action('wp_head', [$this, 'mobileHeaderBar']);
        add_action( 'wp_head', [$this, 'favicon'] );
        // add_action('wp_head', [$this, 'metaTags']);
    }


    
   public function metaTags()
   {
   
       $title = get_bloginfo('name') ?: "DVB - Multimedia Group";
       $description = get_bloginfo('description') ?: "DVB TV Channel - 24/7 Daily Television, and Online News Network in Myanmar/Burma. The DVB (Democratic Voice of Burma) is a non-profit Burmese media organization committed to responsible journalism.";
       $link = WP_HOME;
       $image = INOFOREST_THEME_DIST_IMG_URL . '/default.png';
       if (is_single() || is_page()) {
           $title          = get_the_title();
           $description    = get_the_excerpt();
           $link           = get_the_permalink();
   
           if (has_post_thumbnail()) {
               $src            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', false);
               $image      = WP_HOME . $src[0];
           }
       } elseif (is_category() || is_tag()) {
   
           $obj = get_queried_object();
   
           $title          = $obj->name;
   
           if (!empty($obj->category_description)) {
               $description    = $obj->category_description;
           }
   
           if (is_tax()) {
               $link           = WP_HOME . '/category/' . $obj->slug;
           } else {
               $link           = WP_HOME . '/tag/' . $obj->slug;
           }
       }
   
   ?>
       <!-- Primary Meta Tags -->
       <title><?php echo $title; ?></title>
       <meta name="title" content="<?php echo $title; ?>">
       <meta name="description" content="<?php echo $description; ?>">
   
       <!-- Open Graph / Facebook -->
       <meta property="og:type" content="website">
       <meta property="og:url" content="<?php echo $link; ?>">
       <meta property="og:title" content="<?php echo $title; ?>">
       <meta property="og:description" content="<?php echo $description; ?>">
       <meta property="og:image" content="<?php echo $image; ?>">
   
       <!-- Twitter -->
       <meta property="twitter:card" content="summary_large_image">
       <meta property="twitter:url" content="<?php echo $link; ?>">
       <meta property="twitter:title" content="<?php echo $title; ?>">
       <meta property="twitter:description" content="<?php echo $description; ?>">
       <meta property="twitter:image" content="<?php echo $image; ?>">
   
   <?php
   }


    public function mobileHeaderBar()
    {
        echo '<meta name="theme-color" content="'. INOFOREST_PRIMARY_COLOR .'">';
        echo '<meta name="msapplication-navbutton-color" content="'. INOFOREST_PRIMARY_COLOR .'">';
        echo '<meta name="apple-mobile-web-app-status-bar-style" content="' . INOFOREST_PRIMARY_COLOR .'">';
    }


    public function favicon()
    {
        $url = InoforestHelpers::file_url_with_version(INOFOREST_THEME_IMG.'/favicon.ico');
        echo '<link rel="shortcut icon" href="'. $url .'" type="image/x-icon" />';
        echo '<link rel="icon" href="'. $url .'" type="image/x-icon">';
    }

}
new InoforestHook();