<?php

if (!defined('ABSPATH')) {
    die('Direct access forbidden');
}


require_once get_template_directory() . '/inc/plugins/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/inc/plugins/Mobile_Detect.php';


class InoforestPluginActivation
{


    public function __construct()
    {
        add_action('tgmpa_register', array($this, 'pluginRegister'));
    }


    function pluginRegister()
    {

        if (function_exists('required_plugins')) {
            $required_plugins = required_plugins();
        } else {
            $required_plugins = array(
                array(
                    'name' => 'The VarDumper Component',
                    'slug' => 'inoforest-var',
                    'soruce' => 'https://github.com/hlaporthein/inoforest-var/archive/master.zip',
                    'required' => false,
                ),
                array(
                    'name' => 'Fly Image Crop',
                    'slug' => 'fly-image-crop',
                    'soruce' => 'https://github.com/hlaporthein/fly-image-crop/archive/master.zip',
                    'required' => true,
                ),
                array(
                    'name' => 'Inoforest Login Attempts',
                    'slug' => 'inoforest-login-attempts',
                    'soruce' => 'https://github.com/hlaporthein/fly-image-crop/archive/master.zip',
                    'required' => true,
                )
            );
        }

        tgmpa($required_plugins);
    }
}

$theme_required_plugin_activate = new InoforestPluginActivation();
