<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}

class InoforestStatic {


    public function __construct() {
        add_action('wp_enqueue_scripts', [$this, 'site_google_embeded_font']);
        add_action('wp_head', [$this, 'mm_fonts_embeded'], 0);
        add_action('wp_enqueue_scripts', [$this, 'css']);
        add_action('wp_enqueue_scripts', [$this, 'js']);
        add_action('enqueue_block_editor_assets', [$this, 'guternbergEditorStyle']);
    }

    function guternbergEditorStyle() {
        wp_enqueue_style('editor-style', INOFOREST_THEME_FOLDER_URL . '/editor.css', [], INOFOREST_VERSION, 'all');
        wp_enqueue_style('backend-editor-style', INOFOREST_THEME_FOLDER_URL . '/backend-editor.css', [], INOFOREST_VERSION, 'all');
    }


    public function css()
    {
        wp_enqueue_style('inoforest-parent-style', INOFOREST_THEME_FOLDER_URL . '/style.css', [], INOFOREST_VERSION, 'all');
    }

    public function js()
    {
        wp_enqueue_script('inoforest-parent-script', INOFOREST_THEME_JS_URL . '/app.js',[],INOFOREST_VERSION, true);
        wp_localize_script( 'inoforest-parent-script', 'inoforest', array(
            'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'
        ) );
     
         wp_enqueue_script( 'inoforest-parent-script' );
    }


    public function site_google_embeded_font()
    {
        // if ( ENV !== "pro" ) {
            wp_enqueue_style('roboto-condensed-google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,400;0,700;1,400&display=swap');
        // }
    }


    public function mm_fonts_embeded()
    {
        $font_url = INOFOREST_THEME_DIST_FONTS_URL;
        ?>
        <style>
            @font-face {
                font-family: 'Zawgyi-One';
                src: url('<?php echo $font_url; ?>/Zawgyi-One/Zawgyi-One.eot?#iefix') format('embedded-opentype'), url('<?php echo $font_url; ?>/Zawgyi-One/Zawgyi-One.woff') format('woff'), url('<?php echo $font_url; ?>/Zawgyi-One/Zawgyi-One.ttf') format('truetype');
                font-weight: normal;
                font-style:normal
            }

            @font-face {
                font-family: 'MyanmarAngoun';
                src: url('<?php echo $font_url; ?>/MyanmarAngoun/MyanmarAngoun.eot?#iefix') format('embedded-opentype'), url('<?php echo $font_url; ?>/MyanmarAngoun/MyanmarAngoun.woff') format('woff'), url('<?php echo $font_url; ?>/MyanmarAngoun/MyanmarAngoun.ttf') format('truetype');
                font-weight: normal;
                font-style:normal
            }

            @font-face {
                font-family: 'NotoSansMyanmar-Regular';
                src: url('<?php echo $font_url; ?>/NotoSansMyanmar/NotoSansMyanmar-Regular.eot?#iefix') format('embedded-opentype'), url('<?php echo $font_url; ?>/NotoSansMyanmar/NotoSansMyanmar-Regular.woff') format('woff'), url('<?php echo $font_url; ?>/NotoSansMyanmar/NotoSansMyanmar-Regular.ttf') format('truetype');
                font-weight: normal;
                font-style: normal
            }
        </style>

        <?php
    }
}
new InoforestStatic();