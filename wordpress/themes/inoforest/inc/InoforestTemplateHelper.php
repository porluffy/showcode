<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}

class InoforestTemplateHelper {

    public static function slideInMessage($msg, $type, $position = 'slide-in bottom-right') {
        return '
        <div class="alert alert-'.$type .' alert-dismissible fade show  '. $position .'" role="alert">
        '. $msg .'
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>';
    }

    public static function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true, $params = [] ) {
        if ( null === $wp_query ) {
            global $wp_query;
        }
    
        $add_args = [];
    
        //add query (GET) parameters to generated page URLs
        /*if (isset($_GET[ 'sort' ])) {
            $add_args[ 'sort' ] = (string)$_GET[ 'sort' ];
        }*/
    
        $pages = paginate_links( array_merge( [
                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'format'       => '?paged=%#%',
                'current'      => max( 1, get_query_var( 'paged' ) ),
                'total'        => $wp_query->max_num_pages,
                'type'         => 'array',
                'show_all'     => false,
                'end_size'     => 3,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => __( '« Prev' ),
                'next_text'    => __( 'Next »' ),
                'add_args'     => $add_args,
                'add_fragment' => ''
            ], $params )
        );
    
        if ( is_array( $pages ) ) {
            //$current_page = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );
            $pagination = '<div class="pagination"><ul class="pagination">';
    
            foreach ( $pages as $page ) {
                $pagination .= '<li class="page-item' . (strpos($page, 'current') !== false ? ' active' : '') . '"> ' . str_replace('page-numbers', 'page-link', $page) . '</li>';
            }
    
            $pagination .= '</ul></div>';
    
            if ( $echo ) {
                echo $pagination;
            } else {
                return $pagination;
            }
        }
    
        return null;
    }    


    public static function imageTemplate($attachment_id, $width, $height, $args = [])
    {
        $image_class = $args['img_class'] ?? 'full-width-img';
        $wrapper_class = $args['wrapper_class'] ?? 'feature-image-wrapper';
        $link = $args['link'] ?? '#';
        if (get_permalink($attachment_id) && function_exists('ino_fic_crop_image_url')) {
            $image_url = ino_fic_crop_image_url($attachment_id, $width, $height);
            $img_tag = '<img class="' . $image_class . ' ease4" src="' . $image_url . '" />';

            if (!empty($args['post_link']) && $args['post_link'] === true) {
                return '<a href="'. $link .'" class="' . $wrapper_class . '">'.
                $img_tag
                .'</a>';
            } else {
                return '<div class="' . $wrapper_class . '">'.
                $img_tag
                .'</div>';
            }
        }
    }
        
}