<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}



class InoforestThemeSetup {

    function __construct()
    {
        add_action( 'after_setup_theme',array( $this, 'themeSupport' ) );
        add_action( 'widgets_init',array( $this, 'widget_register' ) );
        self::init();
        
    }


    public static function init()
    {
        InoforestHelpers::include_all_files(INOFOREST_THEME_FOLDER_ABSOLUTE_PATH . '/inc/test');
        InoforestHelpers::include_all_files_with_subdir(INOFOREST_THEME_FOLDER_ABSOLUTE_PATH.'/website', [
            'excluded_dir' => ['page-templates']
        ]);
        InoforestHelpers::include_all_files_with_subdir(INOFOREST_THEME_FOLDER_ABSOLUTE_PATH.'/inc/components');
    }

    public function widget_register() {

        if ( function_exists('websiteRegisterSidebar') ) {
            $sidebars = websiteRegisterSidebar();

            if ( !empty($sidebars) ) {
                foreach($sidebars as $sidebar ) {
                    register_sidebar($sidebar);
                }
            }
        }
    }

    function themeSupport() {

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );


        // Set content-width.
        global $content_width;
        if ( ! isset( $content_width ) ) {
            $content_width = 1170;
        }

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        //Register Nav Menu
        register_nav_menus(
            array(
                'wpb-nav-header' => 'Primary Menu'
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'script',
                'style',
            )
        );

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Twenty Twenty, use a find and replace
         * to change 'twentytwenty' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'inoforest' );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        //Post Formats
        add_theme_support('post-formats', array('gallery','status','video'));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );
        // Set post thumbnail size.
        //set_post_thumbnail_size( 1200, 9999 );
    }

}

$theme_setup = new InoforestThemeSetup();