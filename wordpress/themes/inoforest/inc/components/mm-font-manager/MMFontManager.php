<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}


class MMFontManager {

    function __construct()
    {
        add_action('wp_head', [$this, 'switcherCSS']);
    }

    public function switcherCSS()
    {
        ?>
            <style>
                .font-toggle {
                color: #fff;
                }
                .font-toggle input[type=checkbox] {
                display: none;
                min-width: 60px;
                cursor: pointer;
                }
                .font-toggle label {
                position: relative;
                margin-bottom: 0;
                }
                .font-toggle input[type=checkbox] + label::before {
                cursor: pointer;
                content: " ";
                display: block;
                height: 15px;
                width: 30px;
                border: 1px solid #fff;
                border-radius: 9px;
                position: absolute;
                top: 5px;
                left: -33px;
                background: #fff;
                }
                .font-toggle input[type=checkbox] + label::after {
                cursor: pointer;
                content: " ";
                display: block;
                height: 13px;
                width: 13px;
                border: 1px solid #FF686B;
                border-radius: 50%;
                position: absolute;
                top: 6px;
                left: -32px;
                background: #FF686B;
                transition: all 0.3s ease-in;
                }
                .font-toggle input[type=checkbox]:checked + label::after {
                left: -18px;
                transition: all 0.3s ease-in;
                }        
            </style>
        <?php
    }
    

    public static function swicherTag()
    {
        ?>
        <div class="mm-font-switcher font-toggle"> 
            <input type="checkbox" id="temp" checked=""> 
            <label for="temp"> <span class="f-text">Unicode</span> </label>
        </div>        

        <?php
    }

}
new MMFontManager();