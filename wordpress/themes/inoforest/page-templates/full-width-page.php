<?php
/**
 * Template Name: Full Width Page
 */
get_header();
$post_id = get_the_ID();
?>

<div id="default-page-content-section" class="std-section">

<div class="container">

<div class="page-content-container">

<?php 
headline(get_the_title($post_id));

if (has_post_thumbnail($post_id)) {
    $attachment_id = get_post_thumbnail_id($post_id);
    $img_url = ino_fic_crop_image_url($attachment_id, 1200, 500);
    echo '<img class="full-width-img md-mb" src="'. $img_url .'" />';
}

?>


<div class="std-post-content editor-styles-wrapper">
    <?php the_content(); ?>
</div>

</div>

</div>

</div>

<?php
get_footer();

