<div class="searchform-wrapper">
	
	<form action="/" method="get">

		<div class="input-group mb-3">
		  <input type="text" class="form-control" name="s" placeholder="Search" value="<?php the_search_query(); ?>" />
		  <div class="input-group-append">
		    <button class="input-group-text btn btn-primary" id="search-submit">Search</button>
		  </div>
		</div>

	</form>	

</div>
