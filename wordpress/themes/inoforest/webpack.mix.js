const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'assets/js')
    .sass('resources/sass/style.scss', 'style.css').options({
        processCssUrls: false
    })
    .sass('resources/sass/editor.scss', 'editor.css').options({
        processCssUrls: false
    });
