<?php

if (!defined('ABSPATH')) {
    die('Direct access forbidden');
}


// Add Body Class to your custom template
 
add_filter( 'body_class', 'custom_body_class' );
function custom_body_class( $classes ) {

    $detect = new Mobile_Detect;
    $mobile = $detect->isMobile();
    $tablet = $detect->isTablet();
 
    if ( $mobile ) {
        $classes[] = 'mobile-screen';
        if ( $tablet ) {
            $classes[] = 'tablet-screen';
        } else {
            $classes[] = 'phone-screen';
        }
    } else {
        $classes[] = 'desktop-screen';
    }
    return $classes;
 
    return $classes;
}