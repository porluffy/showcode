Theme Activation
------------------------------------------------------------
- You would need to install 3 plugins from plugins folder. And paste in your wordpress website plugin folder.

- copy *inoforest* from themes folder and paste in your wordrpess website theme folder.

- That's it. And you could start to develop



Theme CSS & JS
------------------------------------------------------------
If you would like to use npm, you could use below features
- you could use sass through resouces -> sass -> websites 
- you could add your js files in resouces -> js  -> app.js and then you could add vue, react, angular and vanilla javascript



Theme Mode
------------------------------------------------------------
You could set current theme mode like development or production. Development mode won't be cached and would be going with unique version number.



Development
------------------------------------------------------------
Let's say, you would like to add header section, footer section, archive page or whatever see action list in inoforest/inc/InoforestAction.php, 

- You would see all the actions list in that file? So, let's create header section now, go to inforest->website -> create header.php and add like below code

`
<?php

if ( !defined('ABSPATH') ) {
    die('Direct access forbidden');
}


function top_bar() {
// Add your html codes or logic code.
}
add_action('inoforest_header', 'top_bar', 6);


function add_your_css_or_js_action_here() {
// Add css or js for only header section.
}
add_action('wp_enqueue_script', 'add_your_css_or_js_action_here', 6);


function add_your_ajax_script_for_only_header_section() {
// Add your html codes or logic code.
}
add_action('wp_enqueue_script', 'add_your_ajax_script_for_only_header_section', 6);

`

You see above header.php codes, we could add all business logic, css, js, ajax and everthing for header.php. It is like Javascript frontend framework like vue, react and angular. We could add client site features later.



Purpos Of This Theme
------------------------------------------------------------
- inforest/website folder is for developing your client's website 
- the rest files are for theme framework to develop more easily.